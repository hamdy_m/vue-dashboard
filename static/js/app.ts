// tslint:disable: ordered-imports
import Vue from "vue"
import BootstrapVue from "bootstrap-vue"
import "bootstrap/dist/css/bootstrap.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import App from "@client/components/App.vue"

Vue.use(BootstrapVue)

const app = new Vue({
    components: {
        App,
    },
    el: "#app",
})

export default app
