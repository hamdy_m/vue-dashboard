import * as sinon from "sinon"
import UserManager from "@client/util/UserManager"


describe("UserManager", () => {
    it("should post to /login URL when login() is called", () => {
        const fetchStub = sinon.stub()
        const loginCallbackSpy = sinon.spy()
        const um = new UserManager(fetchStub)
        um.login("test", loginCallbackSpy)
        sinon.assert.calledWith(fetchStub, "/login", {method: "POST", done: loginCallbackSpy, data: {username: "test"}})
    })
})
