import fetchWrapper from "./FetchWrapper"

export default class UserManager {
    private fetchAPI: any
    public constructor(fetchAPI?: any) {
        this.fetchAPI = fetchAPI || fetchWrapper
    }

    public login(username: string, callback: IResponseCallback) {
        this.fetchAPI("/login", {
            data: {username},
            done: callback,
            method: "POST",
        })
    }

    public logout(callback: IResponseCallback) {
        this.fetchAPI("/logout", {done: callback})
    }
}

interface IUserResponse {
    message: string
}

type IResponseCallback = (res: IUserResponse) => void
