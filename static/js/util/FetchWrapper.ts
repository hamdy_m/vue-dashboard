export default function fetchWrapper(url: string, config: IFetchConfig) {
    fetch(url, {
        body: JSON.stringify(config.data),
        method: config.method || "GET",
    }).then((response: Response) => {
        response.json().then(config.done)
    })
}

interface IFetchConfig {
    done: (json: any) => void
    method?: string
    data?: any
}
