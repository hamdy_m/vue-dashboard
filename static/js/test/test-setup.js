const sinon = require("sinon")
require("jsdom-global")()

sinon.addBehavior("callsCallback", (fake, data) => {
    fake.callsFake((url, callback) => {
        callback(data)
    })
})
