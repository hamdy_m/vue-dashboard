import UserManager from "@client/util/UserManager"
import authStore from "./Auth"

class GoogleAPI {

    public renderSigninButton(id: string) {
        gapi.load("auth2", () => {
            gapi.auth2.init({
                client_id: "919726007332-qlvemt9g5i8ul5m78o73f8kk1otegrvt.apps.googleusercontent.com",
            }).then(() => {
                gapi.signin2.render(id, {
                    longtitle: true,
                    onsuccess: this.onSigninSuccess,
                    theme: "dark",
                })
            })
        })
    }

    public signOut(callback?: () => void) {
        gapi.auth2.getAuthInstance().signOut().then(() => {
            new UserManager().logout(() => {
                authStore.signOut()
                if (callback) {
                    callback()
                }
            })
        })
    }

    private onSigninSuccess(user: gapi.auth2.GoogleUser) {
        const profile = user.getBasicProfile()
        new UserManager().login(profile.getName(), (res) => {
            authStore.setSignedIn()
            authStore.setUserName(profile.getName())
        })
    }

}

export default new GoogleAPI()
