export default {
    user: {
        name: "",
        signedIn: false,
    },
    setUserName(name: string) {
        this.user.name = name
    },
    setSignedIn() {
        this.user.signedIn = true
    },
    signOut() {
        this.user.signedIn = false
        this.user.name = ""
    },
}
