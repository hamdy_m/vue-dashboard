import fetchWrapper from "@client/util/FetchWrapper"

class CallsStore {
    public calls: ICall[]
    public refreshInterval: number
    private fetchAPI: any
    public constructor(fetchAPI?: any) {
        this.fetchAPI = fetchAPI || fetchWrapper
        this.calls = []
        this.refreshInterval = 10
    }

    public getCalls(): Promise<ICallsResponse> {
        return new Promise((resolve: (data: any) => void) => {
            this.fetchAPI("/calls", {done: (data: ICallsResponse) => {
                this.calls = data.calls
                this.refreshInterval = data.refresh_interval
                resolve(data)
            }})
        })
    }

    public updateRefreshInterval(value: number) {
        this.fetchAPI("/update-interval", {
            data: {value},
            done: () => {
                this.refreshInterval = value
            },
            method: "PUT",
        })
    }
}

interface ICall {
    firstname: string,
    status: string,
    duration: number,
    calls_taken: number
}

interface ICallsResponse {
    refresh_interval: number,
    calls: ICall[]
}

export default new CallsStore()
