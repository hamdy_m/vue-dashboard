import { shallowMount } from "@vue/test-utils"
import * as chai from "chai"
import * as sinon from "sinon"
import SignInButton from "./SignInButton.vue"

const expect = chai.expect

describe("SignInButton", () => {
    it("should call renderSigninButton() from the GoogleAPI", () => {
        const renderSigninButtonStub = sinon.stub()
        const mockGoogleAPI = {renderSigninButton: renderSigninButtonStub}
        const component = shallowMount(SignInButton, {
            data: {
                googleAPI: mockGoogleAPI,
            },
        })
        sinon.assert.calledWith(renderSigninButtonStub, "g-signin")
    })
})
