import { shallowMount } from "@vue/test-utils"
import * as chai from "chai"
import * as sinon from "sinon"
import CallTable from "./CallTable.vue"

const expect = chai.expect

describe("CallTable component", () => {
    it("should request call data if user is signed in", () => {
        const fakeResponse = {
            calls: [
                {
                    calls_taken: 321,
                    duration: 123,
                    firstname: "test",
                    status: "test",
                },
            ],
            refresh_interval: 10,
        }

        const stubGetCalls = sinon.stub().resolves(fakeResponse)
        const callTable = shallowMount(CallTable, {
            computed: {
                shouldRequestData() {
                    return true
                },
            },
            data: {
                callsStore: {getCalls: stubGetCalls},
            },
        })
        expect(true).to.be.eq(true)
    })
})
