const exec = require("child_process").exec
const gulp = require("gulp")
const webpack = require("webpack")
const tslint = require("gulp-tslint")
const gulpUtil = require("gulp-util")

gulp.task("python:lint", (cb) => {
    exec("flake8", (err, stdout, stderr) => {
        console.log(stdout)
        console.log(stderr)
        cb()
    })
})

gulp.task("python:lint:watch", ["python:lint"], () => {
    gulp.watch("**/*.py", ["python:lint"])
})

gulp.task("python:test", (cb) => {
    exec("pytest dashboard", (err, stdout, stderr) => {
        console.log(stdout)
        console.log(stderr)
        cb()
    })
})

gulp.task("python:test:watch", ["python:test"], () => {
    gulp.watch("**/*.py", ["python:test"])
})

gulp.task("python:server", (cb) => {
    exec("flask run --host=0.0.0.0", (err, stdout, stderr) => {
        console.log(stdout)
        console.log(stderr)
        cb()
    })
})

gulp.task("js:webpack", () => {
    webpack(require("./webpack.config"), (err, stats) => {
        if (err) throw new gulpUtil.PluginError("webpack", err)
        gulpUtil.log("[webpack]", stats.toString("normal"))
    })
})

gulp.task("js:webpack:watch", ["js:webpack"], () => {
    gulp.watch(["./static/js/**/*.*"], ["js:webpack"])
})

gulp.task("ts:lint", () => {
    gulp.src("./static/js/**/*.ts")
        .pipe(tslint({
            formatter: "verbose"
        }))
        .pipe(tslint.report())
})

gulp.task("ts:lint:watch", ["ts:lint"], () => {
    gulp.watch("./static/js/**/*.ts", ["ts:lint"])
})

gulp.task("js:test", (cb) => {
    exec("npm test", (err, stdout, stderr) => {
        console.log(stdout)
        console.log(stderr)
        cb()
    })
})

gulp.task("js:test:watch", ["js:test"], () => {
    gulp.watch("./static/js/**/*.*", ["js:test"])
})

gulp.task("default", ["python:lint:watch", "python:test:watch", "python:server", "js:webpack:watch", "ts:lint:watch", "js:test:watch"])
