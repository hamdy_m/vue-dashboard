# Vue Dashboard

## Run the app
1. Activate your virtualenv/interpreter
2. `pip install -r requirements.txt`
3. `npm install`
4. `npm run build`
5. Open [this url](http://localhost:5000)
6. Profit!

## Testing the Google sign in button
You can't test the Google sign in button on localhost. To use it, install [ngrok](https://ngrok.com/download) 
and run it like this

`ngrok http 500`

And copy the **https://...** URL that is printed. Paste it into the browser URL bar.
