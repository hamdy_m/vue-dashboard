from flask_restful import fields


class IntervalField(fields.Raw):

    def format(self, value):
        try:
            return value.total_seconds()
        except ValueError as e:
            raise fields.MarshallingException(e)
