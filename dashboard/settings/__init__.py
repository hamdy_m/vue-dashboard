import os.path as path

BASE_DIR = path.abspath(path.dirname(path.dirname(__file__)))

SQLALCHEMY_DATABASE_URI = "sqlite:///" + \
    path.join(BASE_DIR, "data", "sqlite.db")

SECRET_KEY = b'\x08CW(k\x9e\xe8.#\x13\xab\xae\xfd\xae\xaar'
