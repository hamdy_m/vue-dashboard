import csv
import os.path as path
from dashboard.settings import BASE_DIR


def read_calls_csv():
    """Return results as list of dicts"""
    csv_path = path.join(BASE_DIR, "data", "calls.csv")
    with open(csv_path) as csv_file:
        reader = csv.DictReader(csv_file)
        return list(map(_convert_row_duration, reader))


def _convert_row_duration(row):
    row["duration"] = float(row["duration"])
    return row
