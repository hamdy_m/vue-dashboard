from flask import Flask, render_template, request, session, jsonify
from flask_restful import Api, Resource
from flask_sqlalchemy import SQLAlchemy
from dashboard.utils import read_calls_csv

app = Flask(__name__, template_folder="../dist", static_folder="../dist")
app.config.from_object("dashboard.settings")

db = SQLAlchemy(app)
api = Api(app)

from .models import Config  # noqa: E402


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/login", methods=["POST"])
def login():
    session["username"] = request.get_json(force=True)["username"]
    return jsonify({"message": "User logged in successfully"})


@app.route("/logout", methods=["GET", "POST"])
def logout():
    session.pop("username", None)
    return jsonify({"message": "User signed out successfully"})


class CallListView(Resource):

    def get(self):
        return {
            "refresh_interval": Config.query.first()
            .call_table_refresh_interval,
            "calls": read_calls_csv()
        }


class UpdateRefreshIntervalView(Resource):

    def put(self):
        new_refresh_interval = request.get_json(force=True)["value"]
        config = Config.query.first()
        config.call_table_refresh_interval = new_refresh_interval
        db.session.add(config)
        db.session.commit()
        return {"message": "success"}


api.add_resource(CallListView, "/calls")
api.add_resource(UpdateRefreshIntervalView, "/update-interval")
