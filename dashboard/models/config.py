from ..app import db


class Config(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # seconds
    call_table_refresh_interval = db.Column(db.Integer)
