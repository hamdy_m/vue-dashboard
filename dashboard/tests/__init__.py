from dashboard.app import db
from dashboard.models import Config


def create_config():
    config = Config(call_table_refresh_interval=10)
    db.session.add(config)
    db.session.commit()
