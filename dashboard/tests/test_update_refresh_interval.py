import json
from dashboard.models import Config
from . import create_config


def test_update_refresh_interval(client):
    create_config()
    response = client.put("/update-interval", data=json.dumps({
        "value": 1234
    }), content_type='application/json')
    data = json.loads(response.data)
    config = Config.query.first()
    assert config.call_table_refresh_interval == 1234
    assert response.status_code == 200
    assert data["message"] == "success"
