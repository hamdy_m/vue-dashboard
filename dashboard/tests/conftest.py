import os
import tempfile
import pytest
from dashboard.app import app, db


@pytest.fixture
def client():
    temp_fd, temp_filename = tempfile.mkstemp()
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + temp_filename
    app.config["TESTING"] = True
    db.create_all()
    yield app.test_client()
    os.close(temp_fd)
    os.unlink(temp_filename)
