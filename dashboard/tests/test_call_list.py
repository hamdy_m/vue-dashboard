from unittest.mock import patch
import json
from . import create_config


def test_returns_calls(client):
    with patch("dashboard.app.read_calls_csv") as read_calls_csv:
        read_calls_csv.return_value = [{
            "firstname": "test",
            "duration": 10,
            "status": "Waiting",
            "calls_taken": 42
        }]
        create_config()
        response = client.get("/calls")
        data = json.loads(response.data)
        assert data["refresh_interval"] == 10
        assert len(data["calls"]) == 1
        call = data["calls"][0]
        assert call["firstname"] == "test"
        assert call["duration"] == 10
