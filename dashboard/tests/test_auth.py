import json
from unittest.mock import patch


def test_login(client):
    # session testing
    with patch("dashboard.app.session", dict()) as session:
        response = client.post("/login", data={
            "username": "test"
        }, content_type="application/json")
        assert session["username"] == "test"
        # response testing
        data = json.loads(response.data)
        assert data["message"] == "User logged in successfully"
        assert response.status_code == 200


def test_logout(client):
    # log in first
    login(client, "test")
    with client.session_transaction("/logout") as session:
        assert session.get("username") is None
    response = client.post("/logout")
    data = json.loads(response.data)
    assert data["message"] == "User signed out successfully"
    assert response.status_code == 200


def login(client, username):
    return client.post("/login", data={
        "username": username
    })
